var result;
var doc = new jsPDF();
var startTime;
var selectedPlayer;
var playerchanged = false;

$(document).ready(function () {
    $(".nav-link").trigger('click');
    $(".nav-link").hide();
    initializeCanvas();

});

$("#loadCsv").click(function () {
    playerchanged=false;
    startTime = Date.now();
    analyzeCsvFile();
});

$('#players').on('change', function() {
    playerchanged = true;
  })

$("#exportPdf").click(function () {
    if(result==null){
        $.notify("Please click to analyze button first", "warning");
        return; 
    }
    if(playerchanged==true){
        $.notify("Player changed.Click analyze button first", "warning");
        return;        
    }
    var pdf = new jsPDF();
    pdf.text(100, 8, $("#players option:selected").text()); 
    var imgData = document.getElementsByTagName('canvas')[0].toDataURL("image/png", 1.0);
    

    pdf.addImage(imgData, 'JPEG', 40, 10);

    var headers = [{
        "name": "Region",
        "prompt": "Region",
        "width": 35,
        "align": "right",
        "padding": 0,
        "spacing": 0
    }, {
        "name": "NumberofShots",
        "prompt": "Number of Shots",
        "width": 65,
        "align": "left",
        "padding": 0,
        "spacing": 0
    }, {
        "name": "SuccessRate",
        "prompt": "Success Rate",
        "width": 65,
        "align": "left",
        "padding": 0,
        "spacing": 0
    }];

    var row = {};
    var rows = [];
    selectedPlayer = $("#players option:selected").val();
    for ($u = 1; $u <= 14; $u++) {
        var row = {};
        row["Region"] = $u;
        row["NumberofShots"] = result[selectedPlayer][$u]["totalshot"];
        row["SuccessRate"] = result[selectedPlayer][$u]["success"];
        rows.push(row);

    }
    pdf.table(45, 120, rows, headers, {
        autoSize: false,
        printHeaders: true,
        rowMargin: 0,
        fontSize: 8,
        cellpadding: 0,
        cellspacing: 0
    });
    pdf.save("download.pdf");
});

function analyzeCsvFile() {
    if(result==null){
    var file = {};
    file.fileName = $("#fileName").val();
    file.headerRow = $("#headerRow").val();

    $.ajax({
        type: 'POST',
        data: JSON.stringify(file),
        contentType: 'application/json',
        url: 'http://localhost:6009/endpoint',
        success: function (data) {
            var selectedPlayer = $("#players").val();
            drawCanvas(data[selectedPlayer]);
            drawTable(data[selectedPlayer]);
            result = data;
            $.notify("File analyzed.Runtime: "+(Date.now()-startTime)+"ms", "success"); 
        },
        error: function (error) {
            alert("error occured. please contact your system administrator");
        }

    });
}else{
    debugger;
    selectedPlayer = $("#players option:selected").val();
    drawCanvas(result[selectedPlayer]);
    drawTable(result[selectedPlayer]);
    $.notify("File analyzed.Runtime: "+(Date.now()-startTime)+"ms", "success"); 
}
}


function drawTable(data) {
    var table = '<table border="1" align="center">\n';
    table += '<tr><td>Region</td><td>Number of Shots</td><td>Success Rate</td><tr>';
    for ($u = 1; $u <= 14; $u++) {
        table += '<tr>';
        table += '<td>' + $u + '</td>';
        table += '<td>' + data[$u]["totalshot"] + '</td>';
        table += '<td>' + data[$u]["success"] + '</td>';
        table += '</tr>\n';
    }
    table += '</table>';
    $("#mytable").empty();
    $("#mytable").append(table);
}

function drawCanvas(stats) {
    $("#CursorLayer").empty();
    var ctx = document.getElementById('CursorLayer').getContext("2d");
    ctx.beginPath();
        ctx.rect(4, 3, 71, 176);
        ctx.fillStyle = stats[1]["color"];
        ctx.fill(); // region#1
        ctx.fillStyle = "black";
        ctx.font = "30px Arial";
        ctx.fillText("1", 30, 100);
    ctx.beginPath();
        ctx.rect(424, 3, 72, 176);
        ctx.fillStyle = stats[5]["color"];
        ctx.fill();  // region#5
        ctx.fillStyle = "black";
        ctx.font = "30px Arial";
        ctx.fillText("5", 450, 100);
    ctx.beginPath();
        ctx.lineWidth = 0;
        ctx.arc(250, 84, 200, 30 * Math.PI / 180, 61 * Math.PI / 180); //hafif bir pay var
        ctx.lineTo(347, 372);
        ctx.lineTo(496, 372);
        ctx.lineTo(496, 183);
        ctx.closePath(); //?
        ctx.fillStyle = stats[4]["color"];
        ctx.strokeStyle = stats[4]["color"];
        ctx.stroke();
        ctx.fill(); //region#4
        ctx.fillStyle = "black";
        ctx.font = "30px Arial";
        ctx.fillText("4", 415, 300);
    ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.arc(250, 84, 201, 63 * Math.PI / 180, 117 * Math.PI / 180);
        ctx.lineTo(159, 372);
        ctx.lineTo(341, 372);
        ctx.closePath();
        ctx.fillStyle = stats[3]["color"];
        ctx.stroke();
        ctx.fill(); //region#3
        ctx.fillStyle = "black";
        ctx.font = "30px Arial";
        ctx.fillText("3", 245, 335);
    ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.arc(250, 84, 201, 119 * Math.PI / 180, 150.5 * Math.PI / 180);
        ctx.lineTo(4, 183);
        ctx.lineTo(4, 373);
        ctx.lineTo(154, 373);
        ctx.closePath();
        ctx.fillStyle = stats[2]["color"];
        ctx.stroke();
        ctx.fill(); //region#2
        ctx.fillStyle = "black";
        ctx.font = "30px Arial";
        ctx.fillText("2", 70, 290);
    ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.arc(250, 84, 196, 119.5 * Math.PI / 180, 150 * Math.PI / 180);
        ctx.lineTo(79, 119);
        ctx.lineTo(154, 119);
        ctx.closePath();
        ctx.fillStyle = stats[7]["color"];
        ctx.stroke();
        ctx.fill(); //region#7
        ctx.fillStyle = "black";
        ctx.font = "30px Arial";
        ctx.fillText("7", 110, 180);
    ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.arc(250, 84, 195, 62 * Math.PI / 180, 118 * Math.PI / 180);
        ctx.lineTo(159, 212);
        ctx.lineTo(342, 212);
        ctx.closePath();
        ctx.fillStyle = stats[6]["color"];
        ctx.stroke();
        ctx.fill(); //region#6
        ctx.fillStyle = "black";
        ctx.font = "30px Arial";
        ctx.fillText("6", 240, 260);
    ctx.beginPath();
        ctx.lineWidth = 0;
        ctx.arc(250, 84, 195, 30 * Math.PI / 180, 60 * Math.PI / 180);
        ctx.lineTo(348, 119);
        ctx.lineTo(419, 119);
        ctx.closePath(); //?
        ctx.fillStyle = stats[8]["color"];
        ctx.stroke();
        ctx.fill(); //region#8
        ctx.fillStyle = "black";
        ctx.font = "30px Arial";
        ctx.fillText("8", 370, 180);
    ctx.beginPath();
        ctx.rect(159, 118, 90, 90);
        ctx.fillStyle = stats[9]["color"];
        ctx.fill(); // region#9   
        ctx.fillStyle = "black";
        ctx.font = "30px Arial";
        ctx.fillText("9", 195, 170);
    ctx.beginPath();
        ctx.rect(253, 118, 90, 90);
        ctx.fillStyle = stats[10]["color"];
        ctx.fill(); // region#10  
        ctx.fillStyle = "black";
        ctx.font = "30px Arial";
        ctx.fillText("10", 275, 170);
    ctx.beginPath();
        ctx.rect(79, 3, 77, 112);
        ctx.fillStyle = stats[11]["color"];
        ctx.fill(); // region#11 
        ctx.fillStyle = "black";
        ctx.font = "30px Arial";
        ctx.fillText("11", 100, 70);
    ctx.beginPath();
        ctx.rect(160, 3, 89, 112);
        ctx.fillStyle = stats[12]["color"];
        ctx.fill(); // region#12 
        ctx.fillStyle = "black";
        ctx.font = "30px Arial";
        ctx.fillText("12", 185, 70);
    ctx.beginPath();
        ctx.rect(253, 3, 90, 112);
        ctx.fillStyle = stats[13]["color"];
        ctx.fill(); // region#13
        ctx.fillStyle = "black";
        ctx.font = "30px Arial";
        ctx.fillText("13", 280, 70);
    ctx.beginPath();
        ctx.rect(348, 3, 72, 112);
        ctx.fillStyle = stats[14]["color"];
        ctx.fill(); //region#14
        ctx.fillStyle = "black";
        ctx.font = "30px Arial";
        ctx.fillText("14", 360, 70);
}




function initializeCanvas(){
    var canvas = document.createElement('canvas');
    canvas.id = "CursorLayer";
    canvas.width = 500;
    canvas.height = 376;
    canvas.style.zIndex = 8;
    canvas.style.position = "absolute";
    canvas.style.border = "0px solid";
    canvas.style.backgroundColor = "#FFFFFF";
    $("#mycanvas").append(canvas);
}

