// modules
const fs = require('fs');
var express = require('express');
var csv = require("fast-csv");
var bodyParser = require('body-parser');
const napa = require('napajs');
// variables
var app = express();
var stream = fs.createReadStream("FrontEndAssignment.csv");
var stream1 = fs.createReadStream("FrontEndAssignment.csv");
var stream1 = fs.createReadStream("FrontEndAssignment.csv");
var counter = 0;
const NUMBER_OF_WORKERS = 3;
const zone = napa.zone.create('zone', { workers: NUMBER_OF_WORKERS });
var start = Date.now();
var promises = [];
const EventEmitter = require('events').EventEmitter;
const eventEmitter = new EventEmitter();
// eventemitters
eventEmitter.on('csvreaded', function (data,res) {
    prepareDatas(data,res);
  }); 
  eventEmitter.on('promisesCompleted', function (data,res) {
      res.send(data);
  }); 

// request routing
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.post('/endpoint', function(req, res){
    start = Date.now();  
    var obj = readCsv(req.body.fileName,req.body.headerRow,res);    
});
app.listen(6009);

// this method reads the csv file, creates a json object containing 3 json objects
function readCsv(fileName,dataRow,res){
    var alldata = {};
    var record = {}; 
    alldata.player1 = [];
    alldata.player2 = [];
    alldata.player3 = []; //json array to store region&status objects


var csvStream = csv()
    .on("data", function (csvrow) {
            if (counter >= dataRow) {

                record.region = csvrow[0];record.status = csvrow[1];alldata.player1.push(record);record = {}; 
                record.region = csvrow[2];record.status = csvrow[3];alldata.player2.push(record);record = {}; 
                record.region = csvrow[4];record.status = csvrow[5];alldata.player3.push(record);record = {}; 
            }
            counter++;
    })
    .on("end", function () {
        eventEmitter.emit('csvreaded', alldata,res)
    });
stream.pipe(csvStream);
}

function prepareStats(data) {
    var i;
    var regions = {};

    for(i=1;i<=14;i++){
        regions[i] = {"in":0,"out":0,"success":0,"color":"","totalshot":0};
    }

    data.forEach(element => {
        var regionNum = element.region;
        if (element.status == 0)
            regions[regionNum]["out"] = regions[regionNum]["out"]+1;
        else
            regions[regionNum]["in"] = regions[regionNum]["in"]+1;
    });

    for(var element in regions){
            var rate = regions[element]["in"] / (regions[element]["in"] + regions[element]["out"]);
            var ratio = Math.trunc(rate*100);
            regions[element]["success"] = ratio;
            regions[element]["totalshot"] = regions[element]["in"] + regions[element]["out"];

            if(ratio<=10)
                color = "rgb(100%,0%,0%)";
            else if(ratio<=20)
                color = "rgb(90%,10%,0%)";
            else if(ratio<=30)
                color = "rgb(80%,20%,0%)";
            else if(ratio<=40)
                color = "rgb(70%,30%,0%)";
            else if(ratio<=50)
                color = "rgb(60%,40%,0%)";
            else if(ratio<=60)
                color = "rgb(50%,50%,0%)";
            else if(ratio<=70)
                color = "rgb(40%,60%,0%)";
            else if(ratio<=80)
                color = "rgb(30%,70%,0%)";
            else if(ratio<=90)
                color = "rgb(20%,80%,0%)";
            else if(ratio<=100)
                color = "rgb(10%,90%,0%)";

            regions[element]["color"] = color;
    }

    return regions;
}

// this method counts successrates with multithreading paralel processing
// each column corresponds to a thread so there are 3 threads
function prepareDatas(data,res){
    //Slicing longSequence in 4 pieces and sending them to parallel processing
    var parallelism = NUMBER_OF_WORKERS;
    var promises_local = [];
    var index = Object.keys(data);
    console.log(index[0]);

    for(var i = 0; i < parallelism; i++) {
        promises_local.push(zone.execute(prepareStats, [data[index[i]]]));
     }
    Promise.all(promises_local).then(values => {
        values.forEach(result => promises.push(result.value));
        eventEmitter.emit('promisesCompleted', promises,res);
    }).catch(error => console.error(error));
}